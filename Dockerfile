FROM nginx:1-alpine

WORKDIR /usr/share/nginx/html

COPY ./site /usr/share/nginx/html

EXPOSE 80
